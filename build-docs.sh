#!/usr/bin/env bash

set -e

if [ ! -x $(which asciidoctor) ]; then
    echo "Install asciidoctor plz"
    exit 1
fi

if [ ! -x $(which cleancss) ]; then
    echo "npm install clean-css plz"
    exit 1
fi

mkdir -p output

adoc_it() {
    asciidoctor --backend html5 -T templates -D output $1 -o $2
}

mkdir -p output
cleancss docs/style.css -o output/style1.css
cleancss docs/style2.css -o output/style2.css
cat output/style1.css output/style2.css > output/style.css
rm output/style{1,2}.css

for file in docs/*.adoc; do
    adoc_it $file $(basename -s .adoc $file).html
done

adoc_it CONDUCT.adoc conduct.html
adoc_it CONTRIBUTING.adoc contributing.html
adoc_it DCO.txt dco.html

cp output/docs.html output/index.html
