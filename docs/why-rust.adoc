[[what-is-rust-and-why-is-it-useful]]
What is Rust, and why is it useful?
-----------------------------------

https://rust-lang.org[Rust] is a relatively new systems programming language
that tries to achieve the trifecta: fast, safe, and parallel. It has a healthy
mix of features from the popular paradigms, including functional and
object-oriented.

Rust's goals are deeply aligned with those of operating system development.
Its ability to create
http://blog.rust-lang.org/2015/05/11/traits.html[abstraction without overhead]
is immensely useful for operating system development, so that the necessary
unsafety of hardware and other low-level interaction can be contained.
High-level constructs can be used without fear of bloat.

Other safe languages exist, and a few do so with Rust's focus on efficiency.
The real killer is the
http://blog.rust-lang.org/2015/04/10/Fearless-Concurrency.html[fearless
concurrency] that the type system enables.

Plus, Rust is just plain fun to write!
