= New build infrastructure
Corey Richardson <corey@octayn.net>
v1 2016-10-23: Initial revision

As of today, we're now using docker images and xargo instead of running our own
GitLab runners on cmr's desktop.  There's actually still a custom GitLab runner,
but it's purely an optimization (my machine is faster and has good network).
When it's not available, GitLab shared runners will still be used.
Additionally, CI will check that all commits are signed, using some custom
scripts.  The scripts can ignore certain commits (we have a fair amount of
unsigned history), and handles revoked keys specially.  All of this amounts to a
more robust, easier to maintain system, with quick feedback on merge requests
and less configuration needed for people to get up and running with hacking on
Robigalia.
